#!/bin/sh
docker-compose down --volumes
docker system prune -af --volumes

docker-compose up -d
